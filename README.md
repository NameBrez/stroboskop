# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif) 

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://NameBrez@bitbucket.org/NameBrez/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/NameBrez/stroboskop/commits/727dcd59f343b6abbebf51df85461b4689ab1e90 

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/NameBrez/stroboskop/commits/37a58bb657228f9f9477bea2558930c2f2ebe84a

Naloga 6.3.2:
https://bitbucket.org/NameBrez/stroboskop/commits/70d6e9dba521defae676cc048f084e180f0fc71f

Naloga 6.3.3:
https://bitbucket.org/NameBrez/stroboskop/commits/0ada46db4c38434be4d5de89ec6039f70762f0fd

Naloga 6.3.4:
https://bitbucket.org/NameBrez/stroboskop/commits/60ed87ff735a925a108e424759722bb20b9084ff

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/NameBrez/stroboskop/commits/1c4dc378bcc0b01a3c769efd539d64027745f7bd?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/NameBrez/stroboskop/commits/c2fee5e9c980d847c8aebad61f23f18d0d487c76?at=dinamika

Naloga 6.4.3:
https://bitbucket.org/NameBrez/stroboskop/commits/fa2b16d5737b14511496381eb4481094ca0b2547

Naloga 6.4.4:
https://bitbucket.org/NameBrez/stroboskop/commits/8e7e1b61e58fa24e62b5a39a5a038d8208e9410f